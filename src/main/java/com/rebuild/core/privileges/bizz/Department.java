/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.privileges.bizz;

import cn.devezhao.bizz.security.member.BusinessUnit;
import cn.devezhao.persist4j.engine.ID;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;


public class Department extends BusinessUnit {
    private static final long serialVersionUID = -5308455934676294159L;

    public Department(Serializable identity, String name, boolean disabled) {
        super(identity, name, disabled);
    }

    
    public boolean isChildren(Department child, boolean recursive) {
        if (!recursive) return isChildren((ID) child.getIdentity());

        for (BusinessUnit dept : getChildren()) {
            if (dept.getIdentity().equals(child.getIdentity())) {
                return true;
            } else if (((Department) dept).isChildren(child, true)) {
                return true;
            }
        }
        return false;
    }

    
    public boolean isChildren(ID child) {
        for (BusinessUnit dept : getChildren()) {
            if (dept.getIdentity().equals(child)) {
                return true;
            }
        }
        return false;
    }

    
    public Set<BusinessUnit> getAllChildren() {
        Set<BusinessUnit> children = new HashSet<>(getChildren());
        for (BusinessUnit child : getChildren()) {
            children.addAll(((Department) child).getAllChildren());
        }
        return Collections.unmodifiableSet(children);
    }
}